/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taxi.ui;

import java.awt.Color;
import java.awt.Component;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import taxi.db.HibernateUtil;
import taxi.model.Motorista;
import taxi.model.Veiculo;
import org.hibernate.Transaction;
import taxi.model.VeiculoMotorista;



/**
 *
 * @author Daniel
 */
public class TaxiPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form TaxiPrincipal
     */                

    
    public TaxiPrincipal() {
        initComponents();
        listaMotoristas();
        listaVeiculos();
        listaVeiculosMotoristas();
    }
    
    //Executa a Query
    
    private void executeHQLQuery(String hql, int tipo) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query q = session.createQuery(hql);
            List resultList = q.list();
            if(tipo == 1){
                tabelaMotoristas(resultList);
            } else if (tipo == 2){
                tabelaVeiculos(resultList);
            } else if (tipo == 3){
                listaMotoristasTrabalho(resultList);                
            } else if (tipo == 4){
                listaVeiculosTrabalho(resultList);
            } else if (tipo == 5){
                tabelaVeiculosMotoristas(resultList);
            }
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    
    //Query para listar todos os motoristas
    private void listaMotoristas() {
        executeHQLQuery("from Motorista", 1);
    }

    //Atualiza a tabela de motoristas
    private void tabelaMotoristas(List resultList) {
        Vector<String> tableHeaders = new Vector<String>();
        Vector tableData = new Vector();
        tableHeaders.add("Registro"); 
        tableHeaders.add("Nome");
        tableHeaders.add("Endereço");
        tableHeaders.add("Telefone");
        tableHeaders.add("Status");

        for(Object o : resultList) {
            Motorista mot = (Motorista)o;
            Vector<Object> oneRow = new Vector<Object>();
            oneRow.add(mot.getMotRegistro());
            oneRow.add(mot.getMotNome());
            oneRow.add(mot.getMotEndereco());
            oneRow.add(mot.getMotTelefone());
            oneRow.add(mot.getMotStatus());
            tableData.add(oneRow);
        }
        motTab.setModel(new DefaultTableModel(tableData, tableHeaders));
        
        //Escondendo a coluna Status
        motTab.getColumnModel().getColumn(4).setMaxWidth(0);
        motTab.getColumnModel().getColumn(4).setMinWidth(0);
        motTab.getColumnModel().getColumn(4).setPreferredWidth(0);
        
        //Colorindo as linhas de acordo como Status
        motTab.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {  
            public Component getTableCellRendererComponent(JTable table, Object value,  
                    boolean isSelected, boolean hasFocus, int row, int column) {  
                super.getTableCellRendererComponent(table, value, isSelected,  
                        hasFocus, row, column);  
                if (table.getValueAt(row, 4) == 0) {  
                    setBackground(Color.RED);  
                } else if(table.getValueAt(row, 4) == 1) {  
                    setBackground(Color.green);  
                }                  
                return this;  
            }  
        }); 
    }
    
    
    //Query para listar todos os veículos
    private void listaVeiculos() {
        executeHQLQuery("from Veiculo", 2);
    }

    //Atualiza a tabela de veículos
    private void tabelaVeiculos(List resultList) {
        Vector<String> tableHeaders = new Vector<String>();
        Vector tableData = new Vector();
        tableHeaders.add("Placa"); 
        tableHeaders.add("Tipo");
        tableHeaders.add("Marca");
        tableHeaders.add("Ano");
        tableHeaders.add("Cor");
        tableHeaders.add("Status");
        
        String tipo = "";

        for(Object o : resultList) {
            Veiculo vei = (Veiculo)o;
            Vector<Object> oneRow = new Vector<Object>();            
            if (vei.getVeiTipo() == 0) { tipo = "Taxi"; } else if (vei.getVeiTipo() == 1) { tipo = "Transporte Especial"; } else { tipo = "Transporte Executivo"; }            
            oneRow.add(vei.getVeiPlaca());                        
            oneRow.add(tipo);
            oneRow.add(vei.getVeiMarca());
            oneRow.add(vei.getVeiAno());
            oneRow.add(vei.getVeiCor());
            oneRow.add(vei.getVeiStatus());
            tableData.add(oneRow);
            
        }
        veiTab.setModel(new DefaultTableModel(tableData, tableHeaders));
        
        //Escondendo a coluna Status
        veiTab.getColumnModel().getColumn(5).setMaxWidth(0);
        veiTab.getColumnModel().getColumn(5).setMinWidth(0);
        veiTab.getColumnModel().getColumn(5).setPreferredWidth(0);
        
        //Colorindo as linhas de acordo como Status
        veiTab.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {  
            public Component getTableCellRendererComponent(JTable table, Object value,  
                    boolean isSelected, boolean hasFocus, int row, int column) {  
                super.getTableCellRendererComponent(table, value, isSelected,  
                        hasFocus, row, column);  
                if (table.getValueAt(row, 5) == 0) {  
                    setBackground(Color.RED);  
                } else if(table.getValueAt(row, 5) == 1) {  
                    setBackground(Color.green);  
                }  
                
                return this;  
            }  
        }); 
                        
    }
    
    private void listaVeiculosMotoristas() {
        executeHQLQuery("from VeiculoMotorista", 5);
    }
    
    private void tabelaVeiculosMotoristas(List resultList) {
        Vector<String> tableHeaders = new Vector<String>();
        Vector tableData = new Vector();
        tableHeaders.add("ID");
        tableHeaders.add("Status");
        tableHeaders.add("Veículo");
        tableHeaders.add("Placa"); 
        tableHeaders.add("Motorista");        
        tableHeaders.add("Data Retirada");
        tableHeaders.add("Hora Retirada");
        tableHeaders.add("Km Retirada");
        tableHeaders.add("Data Chegada");
        tableHeaders.add("Hora Chegada");
        tableHeaders.add("Km Chegada");

        for(Object o : resultList) {
            VeiculoMotorista veimot = (VeiculoMotorista)o;
            Vector<Object> oneRow = new Vector<Object>();            
            
            oneRow.add(veimot.getVeimotId());
            oneRow.add(veimot.getVeimotStatus());
            oneRow.add(veimot.getVeiculo().getVeiMarca());
            oneRow.add(veimot.getVeiculo().getVeiPlaca());
            oneRow.add(veimot.getMotorista().getMotNome());
            oneRow.add(getDateTime(veimot.getVeimotDataRetirada(),"dd/MM/YYYY"));
            oneRow.add(getDateTime(veimot.getVeimotDataRetirada(),"HH:mm"));
            oneRow.add(veimot.getVeimotKmRetirada());
            if(veimot.getVeimotStatus().equals("c")){
                oneRow.add(getDateTime(veimot.getVeimotDataChegada(),"dd/MM/YYYY"));
                oneRow.add(getDateTime(veimot.getVeimotDataChegada(),"HH:mm"));            
                oneRow.add(veimot.getVeimotKmChegada());
            } else {
                oneRow.add("-");
                oneRow.add("-");            
                oneRow.add("-");
            }
            
            tableData.add(oneRow);            
        }
        
        ctrlTab.setModel(new DefaultTableModel(tableData, tableHeaders));        
        
        ctrlTab.getColumnModel().getColumn(0).setMaxWidth(0);
        ctrlTab.getColumnModel().getColumn(0).setMinWidth(0);
        ctrlTab.getColumnModel().getColumn(0).setPreferredWidth(0);
        ctrlTab.getColumnModel().getColumn(0).setResizable(false);
        
        ctrlTab.getColumnModel().getColumn(1).setMaxWidth(0);
        ctrlTab.getColumnModel().getColumn(1).setMinWidth(0);
        ctrlTab.getColumnModel().getColumn(1).setPreferredWidth(0);
        ctrlTab.getColumnModel().getColumn(1).setResizable(false);
                     
    }    
    
    private void listaMotoristasTrabalho(List resultList){
        if(resultList.isEmpty()){
            ctrlMotorista.removeAllItems();
            ctrlMotorista.setEnabled(false);
            ctrlKmInicial.setEnabled(false);
            ctrlRegistrarSaida.setEnabled(false);
        } else {
            ctrlMotorista.setEnabled(true);
            ctrlRegistrarSaida.setEnabled(true);
            ctrlKmInicial.setEnabled(true);
            for(Object o : resultList) {
                Motorista mot = (Motorista)o;
                ctrlMotorista.addItem(mot);
            }
        }
    }
    
    private void listaVeiculosTrabalho(List resultList){
        if(resultList.isEmpty()){
            ctrlVeiculo.removeAllItems();
            ctrlVeiculo.setEnabled(false);
            ctrlKmInicial.setEnabled(false);
            ctrlRegistrarSaida.setEnabled(false);
        } else {
            ctrlVeiculo.setEnabled(true);
            ctrlRegistrarSaida.setEnabled(true);
            ctrlKmInicial.setEnabled(true);
            for(Object o : resultList) {
                Veiculo vei = (Veiculo)o;
                ctrlVeiculo.addItem(vei);
            }
        }
    }
    
    //Função para limpar os campos do motorita
    private void limpaCamposMotorista(){
        motRegistro.setText("");
        motNome.setText("");
        motEndereco.setText("");
        motTelefone.setText("");
    }
    
    //Função para limpar os campos do veículo
    private void limpaCamposVeiculo(){
        veiPlaca.setText("");
        veiMarca.setText("");
        veiAno.setText("");
        veiCor.setText("");
        veiTipo.setSelectedIndex(0);
    } 
    
    private void limparListasControle(){
        ctrlMotorista.removeAllItems();            
        ctrlVeiculo.removeAllItems();
    }
    
    private void limparCamposControle(){
        ctrlKmInicial.setText("");
        ctrlDataRetirada.setDate(null);
        ctrlHoraRetirada.setText("");
        ctrlKmFinal.setText("");
        ctrlDataChegada.setDate(null);
        ctrlHoraChegada.setText("");
    }
    
    //Formato MySQL YYYY-MM-dd HH:mm:ss
    private String getDateTime(Date date, String formato) {
        DateFormat dateFormat = new SimpleDateFormat(formato);
        return dateFormat.format(date);
    }
    
    
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ctrlTipo = new javax.swing.ButtonGroup();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        painelMotorista = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        motRegistro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        motNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        motEndereco = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        motBtSalvar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        motTab = new javax.swing.JTable();
        motTelefone = new javax.swing.JFormattedTextField();
        motBtExcluir = new javax.swing.JButton();
        painelVeiculo = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        veiBtSalvar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        veiTab = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        veiMarca = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        veiAno = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        veiCor = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        veiPlaca = new javax.swing.JFormattedTextField();
        veiTipo = new javax.swing.JComboBox();
        veiBtExcluir = new javax.swing.JButton();
        painelControle = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        ctrlMotorista = new javax.swing.JComboBox();
        ctrlVeiculo = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        ctrlRegistrarSaida = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        ctrlTab = new javax.swing.JTable();
        jLabel12 = new javax.swing.JLabel();
        ctrlKmInicial = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        ctrlHoraRetirada = new javax.swing.JFormattedTextField();
        jLabel15 = new javax.swing.JLabel();
        ctrlKmFinal = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        ctrlHoraChegada = new javax.swing.JFormattedTextField();
        ctrlRegistrarChegada = new javax.swing.JButton();
        ctrlDataRetirada = new com.toedter.calendar.JDateChooser();
        ctrlDataChegada = new com.toedter.calendar.JDateChooser();
        ctrlExcluir = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem9 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Controle de Frota de Taxi");
        setMinimumSize(new java.awt.Dimension(800, 600));

        jToolBar1.setRollover(true);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconUserSaveGnd.png"))); // NOI18N
        jButton1.setText("Salvar Motorista");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconUserDeleteGnd.png"))); // NOI18N
        jButton2.setText("Excluir Motorista");
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);
        jToolBar1.add(jSeparator1);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconCarSaveGnd.png"))); // NOI18N
        jButton3.setText("Salvar Veículo");
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconCarDeleteGnd.png"))); // NOI18N
        jButton4.setText("Excluir Veículo");
        jButton4.setFocusable(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);
        jToolBar1.add(jSeparator2);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconRegSaidaGnd.png"))); // NOI18N
        jButton5.setText("Registrar Saída");
        jButton5.setFocusable(false);
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton5.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconRegChegadaGnd.png"))); // NOI18N
        jButton6.setText("Registrar Chegada");
        jButton6.setFocusable(false);
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton6);

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconRegExcluitGnd.png"))); // NOI18N
        jButton7.setText("Excluir Controle");
        jButton7.setFocusable(false);
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton7.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton7);

        jLabel1.setText("Registro");

        jLabel2.setText("Nome");

        jLabel3.setText("Endereço");

        jLabel4.setText("Telefone");

        motBtSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconUserSaveGnd.png"))); // NOI18N
        motBtSalvar.setText("Salvar");
        motBtSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                motBtSalvarActionPerformed(evt);
            }
        });

        motTab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(motTab);

        try {
            motTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        motBtExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconUserDeleteGnd.png"))); // NOI18N
        motBtExcluir.setText("Excluir");
        motBtExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                motBtExcluirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelMotoristaLayout = new javax.swing.GroupLayout(painelMotorista);
        painelMotorista.setLayout(painelMotoristaLayout);
        painelMotoristaLayout.setHorizontalGroup(
            painelMotoristaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelMotoristaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelMotoristaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(painelMotoristaLayout.createSequentialGroup()
                        .addComponent(motBtSalvar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(motBtExcluir))
                    .addComponent(jLabel1)
                    .addComponent(motRegistro)
                    .addComponent(jLabel2)
                    .addComponent(motNome)
                    .addComponent(motEndereco)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(motTelefone, javax.swing.GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 743, Short.MAX_VALUE)
                .addContainerGap())
        );
        painelMotoristaLayout.setVerticalGroup(
            painelMotoristaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelMotoristaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelMotoristaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelMotoristaLayout.createSequentialGroup()
                        .addComponent(motRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(motNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(motEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(motTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 228, Short.MAX_VALUE)
                        .addGroup(painelMotoristaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(motBtSalvar)
                            .addComponent(motBtExcluir)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Motoristas", painelMotorista);

        jLabel6.setText("Placa");

        veiBtSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconCarSaveGnd.png"))); // NOI18N
        veiBtSalvar.setText("Salvar");
        veiBtSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                veiBtSalvarActionPerformed(evt);
            }
        });

        veiTab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(veiTab);

        jLabel7.setText("Marca");

        jLabel8.setText("Ano");

        jLabel9.setText("Cor");

        jLabel10.setText("Tipo");

        try {
            veiPlaca.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("UUU-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        veiTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Taxi", "Transporte Especial", "Transporte Executivo" }));

        veiBtExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconCarDeleteGnd.png"))); // NOI18N
        veiBtExcluir.setText("Excluir");
        veiBtExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                veiBtExcluirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelVeiculoLayout = new javax.swing.GroupLayout(painelVeiculo);
        painelVeiculo.setLayout(painelVeiculoLayout);
        painelVeiculoLayout.setHorizontalGroup(
            painelVeiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelVeiculoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelVeiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(painelVeiculoLayout.createSequentialGroup()
                        .addComponent(veiBtSalvar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(veiBtExcluir))
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(veiMarca)
                    .addComponent(veiAno)
                    .addComponent(veiCor)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(veiPlaca)
                    .addComponent(veiTipo, 0, 309, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 743, Short.MAX_VALUE)
                .addContainerGap())
        );
        painelVeiculoLayout.setVerticalGroup(
            painelVeiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelVeiculoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelVeiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelVeiculoLayout.createSequentialGroup()
                        .addComponent(veiPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(veiMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(veiAno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(veiCor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(veiTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 175, Short.MAX_VALUE)
                        .addGroup(painelVeiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(veiBtSalvar)
                            .addComponent(veiBtExcluir)))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Veículo", painelVeiculo);

        painelControle.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                painelControleComponentShown(evt);
            }
        });

        jLabel5.setText("Motorista:");

        jLabel11.setText("Veículo:");

        ctrlRegistrarSaida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconRegSaidaPeq.png"))); // NOI18N
        ctrlRegistrarSaida.setText("Registrar Saída");
        ctrlRegistrarSaida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ctrlRegistrarSaidaActionPerformed(evt);
            }
        });

        ctrlTab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(ctrlTab);

        jLabel12.setText("KM Inicial:");

        jLabel13.setText("Data:");

        jLabel14.setText("Hora:");

        try {
            ctrlHoraRetirada.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        ctrlHoraRetirada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ctrlHoraRetiradaActionPerformed(evt);
            }
        });

        jLabel15.setText("KM Final:");

        jLabel16.setText("Data:");

        jLabel17.setText("Hora:");

        try {
            ctrlHoraChegada.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        ctrlRegistrarChegada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconRegChegadaPeq.png"))); // NOI18N
        ctrlRegistrarChegada.setText("Registrar Retorno");
        ctrlRegistrarChegada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ctrlRegistrarChegadaActionPerformed(evt);
            }
        });

        ctrlExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconRegExcluitPeq.png"))); // NOI18N
        ctrlExcluir.setText("Excluir Controle");
        ctrlExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ctrlExcluirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelControleLayout = new javax.swing.GroupLayout(painelControle);
        painelControle.setLayout(painelControleLayout);
        painelControleLayout.setHorizontalGroup(
            painelControleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelControleLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelControleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelControleLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ctrlMotorista, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ctrlVeiculo, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ctrlKmInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ctrlDataRetirada, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ctrlHoraRetirada, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ctrlRegistrarSaida)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 234, Short.MAX_VALUE))
                    .addComponent(jScrollPane3)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelControleLayout.createSequentialGroup()
                        .addComponent(ctrlExcluir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ctrlKmFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ctrlDataChegada, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ctrlHoraChegada, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ctrlRegistrarChegada)))
                .addContainerGap())
        );
        painelControleLayout.setVerticalGroup(
            painelControleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelControleLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelControleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(painelControleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(ctrlMotorista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11)
                        .addComponent(ctrlVeiculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ctrlRegistrarSaida)
                        .addComponent(jLabel12)
                        .addComponent(ctrlKmInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel13)
                        .addComponent(jLabel14)
                        .addComponent(ctrlHoraRetirada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(ctrlDataRetirada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(painelControleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelControleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(ctrlRegistrarChegada)
                        .addComponent(jLabel16)
                        .addComponent(jLabel17)
                        .addComponent(jLabel15)
                        .addComponent(ctrlKmFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ctrlHoraChegada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ctrlExcluir))
                    .addComponent(ctrlDataChegada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Controle Diário", painelControle);

        jMenu1.setText("Arquivo");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconUserSavePeq.png"))); // NOI18N
        jMenuItem1.setText("Salvar Motorista");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconUserDeletePeq.png"))); // NOI18N
        jMenuItem2.setText("Excluir Motorista");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);
        jMenu1.add(jSeparator3);

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconCarSavePeq.png"))); // NOI18N
        jMenuItem3.setText("Salvar Veículo");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconCarDeletePeq.png"))); // NOI18N
        jMenuItem4.setText("Excluir Veículo");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);
        jMenu1.add(jSeparator4);

        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconRegSaidaPeq.png"))); // NOI18N
        jMenuItem5.setText("Registrar Saída");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconRegChegadaPeq.png"))); // NOI18N
        jMenuItem6.setText("Registrar Chegada");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem6);

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconRegExcluitPeq.png"))); // NOI18N
        jMenuItem7.setText("Excluir Controle");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem7);
        jMenu1.add(jSeparator5);

        jMenuItem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconSair.png"))); // NOI18N
        jMenuItem8.setText("Sair");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem8);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Ajuda");

        jMenuItem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/taxi/ui/imagens/iconAjudaPeq.png"))); // NOI18N
        jMenuItem9.setText("Ajuda");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem9);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void motBtSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_motBtSalvarActionPerformed
        Session sess = null;
        try{
            sess = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = sess.beginTransaction();
            Motorista mot = new Motorista();
            mot.setMotRegistro(motRegistro.getText());
            mot.setMotNome(motNome.getText());
            mot.setMotEndereco(motEndereco.getText());
            mot.setMotTelefone(motTelefone.getText());
            mot.setMotStatus(1);
            sess.save(mot);            
            tx.commit();
            listaMotoristas();
            limpaCamposMotorista();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        finally{
            sess.close();
        }
    }//GEN-LAST:event_motBtSalvarActionPerformed

    private void veiBtSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_veiBtSalvarActionPerformed
        Session sess = null;
        try{
            sess = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = sess.beginTransaction();
            Veiculo vei = new Veiculo();
            vei.setVeiPlaca(veiPlaca.getText());
            vei.setVeiMarca(veiMarca.getText());
            vei.setVeiAno(Integer.parseInt(veiAno.getText()));
            vei.setVeiCor(veiCor.getText());
            vei.setVeiTipo(veiTipo.getSelectedIndex());
            vei.setVeiStatus(1);
            sess.save(vei);            
            tx.commit();
            listaVeiculos();
            limpaCamposVeiculo();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        finally{
            sess.close();
        }
    }//GEN-LAST:event_veiBtSalvarActionPerformed

    private void motBtExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_motBtExcluirActionPerformed
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        Transaction tx = session.beginTransaction();  
        Query q = session.createQuery("from Motorista where mot_registro='"+motTab.getValueAt(motTab.getSelectedRow(), 0)+"'");
        Motorista mot = (Motorista)q.uniqueResult();  
        session.delete(mot);  
        tx.commit();
        listaMotoristas();
        session.close(); 
    }//GEN-LAST:event_motBtExcluirActionPerformed

    private void veiBtExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_veiBtExcluirActionPerformed
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Query q = session.createQuery("from Veiculo where vei_placa='"+veiTab.getValueAt(veiTab.getSelectedRow(), 0)+"'");
        Veiculo vei = (Veiculo) q.uniqueResult();
        session.delete(vei);
        tx.commit();
        listaVeiculos();
        session.close();
    }//GEN-LAST:event_veiBtExcluirActionPerformed

    private void ctrlRegistrarSaidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ctrlRegistrarSaidaActionPerformed
        Motorista mot = (Motorista)ctrlMotorista.getSelectedItem();
        Veiculo vei = (Veiculo)ctrlVeiculo.getSelectedItem();
        
        Session sess = null;
        try{
            DateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dataHoraRetirada = formato.parse(getDateTime(ctrlDataRetirada.getDate(), "YYYY-MM-dd ")+ctrlHoraRetirada.getText()+":00");
            
            sess = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = sess.beginTransaction();
            VeiculoMotorista veimot = new VeiculoMotorista();
            veimot.setMotorista(mot);
            veimot.setVeiculo(vei);
            veimot.setVeimotDataRetirada(dataHoraRetirada);
            veimot.setVeimotKmRetirada(Double.parseDouble(ctrlKmInicial.getText()));
            veimot.setVeimotKmChegada(0.0);
            veimot.setVeimotDataChegada(new Date());
            veimot.setVeimotStatus("r");

            mot.setMotStatus(0);           
            vei.setVeiStatus(0);         
            
            sess.save(veimot);   
            sess.update(mot);
            sess.update(vei);
            tx.commit();
            
            sess.close();
            
            listaVeiculosMotoristas();
            listaMotoristas();
            listaVeiculos();
            
            limparListasControle();
            
            limparCamposControle();

            executeHQLQuery("from Motorista m where m.motStatus = 1", 3);            
            executeHQLQuery("from Veiculo v where v.veiStatus = 1", 4);
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }     
      
    }//GEN-LAST:event_ctrlRegistrarSaidaActionPerformed

    private void painelControleComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_painelControleComponentShown
        limparListasControle();        
        //Motoristas livres
            executeHQLQuery("from Motorista m where m.motStatus = 1", 3);
            
        //Veículos livres
            executeHQLQuery("from Veiculo v where v.veiStatus = 1", 4);
                    
    }//GEN-LAST:event_painelControleComponentShown

    private void ctrlRegistrarChegadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ctrlRegistrarChegadaActionPerformed
              
        try {
            DateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dataHoraChegada = formato.parse(getDateTime(ctrlDataChegada.getDate(), "yyyy-MM-dd")+" "+ctrlHoraChegada.getText()+":00");
            
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            
            Query q = session.createQuery("from VeiculoMotorista where veimot_id="+ctrlTab.getValueAt(ctrlTab.getSelectedRow(), 0));
            
            VeiculoMotorista veimot = (VeiculoMotorista) q.uniqueResult();                                    
            veimot.setVeimotKmChegada(Double.parseDouble(ctrlKmFinal.getText()));
            veimot.setVeimotDataChegada(dataHoraChegada);
            veimot.setVeimotStatus("c");
            
            Motorista mot = (Motorista)veimot.getMotorista();
            Veiculo vei = (Veiculo)veimot.getVeiculo();
            
            mot.setMotStatus(1);
            vei.setVeiStatus(1);

            session.update(veimot);
            session.update(mot);
            session.update(vei);
         
            session.getTransaction().commit();
            session.close();
            
            listaVeiculosMotoristas();
            listaMotoristas();
            listaVeiculos();
            
            limparListasControle();            
            limparCamposControle();

            executeHQLQuery("from Motorista m where m.motStatus = 1", 3);            
            executeHQLQuery("from Veiculo v where v.veiStatus = 1", 4);
        } catch (HibernateException he) {
            he.printStackTrace();
        } catch (ParseException ex) {
            Logger.getLogger(TaxiPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_ctrlRegistrarChegadaActionPerformed

    private void ctrlExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ctrlExcluirActionPerformed
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Query q = session.createQuery("from VeiculoMotorista where veimot_id="+ctrlTab.getValueAt(ctrlTab.getSelectedRow(), 0));
        VeiculoMotorista veimot = (VeiculoMotorista) q.uniqueResult(); 
        
        Motorista mot = (Motorista)veimot.getMotorista();
        Veiculo vei = (Veiculo)veimot.getVeiculo();

        mot.setMotStatus(1);
        vei.setVeiStatus(1);

        session.update(mot);
        session.update(vei);
        session.delete(veimot);
            
        tx.commit();
        listaVeiculos();
        session.close();
        
        listaVeiculosMotoristas();
        listaMotoristas();
        listaVeiculos();

        limparListasControle();            
        limparCamposControle();

        executeHQLQuery("from Motorista m where m.motStatus = 1", 3);            
        executeHQLQuery("from Veiculo v where v.veiStatus = 1", 4);
    }//GEN-LAST:event_ctrlExcluirActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        veiBtSalvarActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        motBtSalvarActionPerformed(evt);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        motBtExcluirActionPerformed(evt);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        veiBtSalvarActionPerformed(evt);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        veiBtExcluirActionPerformed(evt);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        ctrlRegistrarSaidaActionPerformed(evt);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        ctrlRegistrarChegadaActionPerformed(evt);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        ctrlExcluirActionPerformed(evt);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        motBtSalvarActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        motBtExcluirActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        veiBtExcluirActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        ctrlRegistrarSaidaActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        ctrlRegistrarChegadaActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        ctrlExcluirActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        JOptionPane.showMessageDialog(null, "Programa de Frota de Taxi (Projeto 2º Bimestre)\n"
                + "FATEC São José dos Campos\n"
                + "Programação de Aplicativos de Banco de Dados\n"
                + "5º Semestre de Banco de Dados\n"
                + "Primeiro Semestre 2012\n"
                + "Aluno: Daniel Cesario");
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void ctrlHoraRetiradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ctrlHoraRetiradaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ctrlHoraRetiradaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TaxiPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TaxiPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TaxiPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TaxiPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TaxiPrincipal().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser ctrlDataChegada;
    private com.toedter.calendar.JDateChooser ctrlDataRetirada;
    private javax.swing.JButton ctrlExcluir;
    private javax.swing.JFormattedTextField ctrlHoraChegada;
    private javax.swing.JFormattedTextField ctrlHoraRetirada;
    private javax.swing.JTextField ctrlKmFinal;
    private javax.swing.JTextField ctrlKmInicial;
    private javax.swing.JComboBox ctrlMotorista;
    private javax.swing.JButton ctrlRegistrarChegada;
    private javax.swing.JButton ctrlRegistrarSaida;
    private javax.swing.JTable ctrlTab;
    private javax.swing.ButtonGroup ctrlTipo;
    private javax.swing.JComboBox ctrlVeiculo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton motBtExcluir;
    private javax.swing.JButton motBtSalvar;
    private javax.swing.JTextField motEndereco;
    private javax.swing.JTextField motNome;
    private javax.swing.JTextField motRegistro;
    private javax.swing.JTable motTab;
    private javax.swing.JFormattedTextField motTelefone;
    private javax.swing.JPanel painelControle;
    private javax.swing.JPanel painelMotorista;
    private javax.swing.JPanel painelVeiculo;
    private javax.swing.JTextField veiAno;
    private javax.swing.JButton veiBtExcluir;
    private javax.swing.JButton veiBtSalvar;
    private javax.swing.JTextField veiCor;
    private javax.swing.JTextField veiMarca;
    private javax.swing.JFormattedTextField veiPlaca;
    private javax.swing.JTable veiTab;
    private javax.swing.JComboBox veiTipo;
    // End of variables declaration//GEN-END:variables
}
